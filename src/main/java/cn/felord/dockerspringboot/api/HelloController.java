package cn.felord.dockerspringboot.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author dax
 * @since 2019/6/13 23:33
 */
@RestController
@RequestMapping("/docker")
public class HelloController {
   @GetMapping("/hello")
  public Map<String,String> hello(){
      Map<String, String> map = new HashMap<>(3);
           map.put("aud","码农小胖哥");
           map.put("msg","关注我学习更多的原创知识");
           map.put("time", LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYYY-MM-dd HH:MM:ss")));
           return  map;

  }
}
